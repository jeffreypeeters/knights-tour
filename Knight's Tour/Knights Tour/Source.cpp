#include <iostream>
#define N 8
using namespace std;

//Print the current board state.
//Will show all the tiles that are visited and shows the order that the knight moved in.
void printTour(int (&solution)[N][N]){
	cout << "\n";
	int i, j;
	for (i = 0; i < N; i++) {
		for (j = 0; j < N; j++) {
			if (solution[j][i] <= 9){
				cout <<"0"<< solution[j][i] << "--";
			} else {
				cout << solution[j][i] << "--";
			}
		}
		cout << "\n";
	}
}

//Checks wheter or not the move that is being made is possible.
bool isMovePossible(int &nextMoveX, int &nextMoveY, int (&tour)[N][N]) {
	if ((nextMoveX >= 0 && nextMoveX < N) && (nextMoveY >= 0 && nextMoveY < N) && (tour[nextMoveX][nextMoveY] == 0)){
		return true;
	}
	return false;
}

//Function to let the knight move over the board. Keeping track of where the knight moved to and what the moves are he can still make.
//If the knight reaches a tile he can't move forward from the function will return false and thus backtracks to the previous tile to continue checking the moves that weren't made yet.
//If all moves are done and no solution is found findTour will return false.
bool findTour(int (&tour)[N][N], int (&allMoves)[8][2], int &moveX, int &moveY, int* moveCount) {
	int i, nextMoveX, nextMoveY;
	if (*moveCount == N*N - 1) {
		return true;
	}

	//Loop to move through all the moves
	for (i = 0; i < N; i++) {
		nextMoveX = moveX + allMoves[i][0];
		nextMoveY = moveY + allMoves[i][1];

		if (isMovePossible(nextMoveX, nextMoveY, tour)) {
			tour[moveX][moveY] = *moveCount + 1;
			*moveCount = *moveCount +1;
			if (findTour(tour, allMoves, nextMoveX, nextMoveY, moveCount) == true) {
				return true;
			} else {
				tour[moveX][moveY] = 0;
				*moveCount = *moveCount -1;
			}				
		}
	}
	return false;
}

void knightTour() {
	int knightStartY, knightStartX;
	int moveCounter = 0;
	int chessBoard[N][N];
	int knightMoves[8][2] = {
		{ 2, 1 },
		{ 1, 2 },
		{ -1, 2 },
		{ -2, 1 },
		{ -2, -1 },
		{ -1, -2 },
		{ 1, -2 },
		{ 2, -1 }
	};

	for (int i = 0; i < N; i++) {
		for (int j = 0; j < N; j++) {
			chessBoard[j][i] = 0;
		}
	}

	//Asks the user for the position of the knight
	//Knight horizontal position
	cout << "Please give a horizontal number for the knight start on: ";
	cin >> knightStartX;
	while (knightStartX > 8 || knightStartX < 1){
		cout << "Coordinate must be chosen from the numbers 1 through 8: ";
		cin >> knightStartX;
	}
	knightStartX--;

	//Knight vertical position
	cout << "Please give a vertical number for the knight start on: ";
	cin >> knightStartY;
	while (knightStartY > 8 || knightStartY < 1){
		cout << "Coordinate must be chosen from the numbers 1 through 8: ";
		cin >> knightStartY;
	}
	knightStartY--;

	
	if (findTour(chessBoard, knightMoves, knightStartX, knightStartY, &moveCounter) == false) {
		cout << "\nKnight tour does not exist";
	}
	else {
		cout << "\nTour exist ...\n";
		printTour(chessBoard);
	}
}

int main() {
	knightTour();
	return 0;
}